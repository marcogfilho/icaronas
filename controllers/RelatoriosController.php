<?php
 
namespace app\controllers;
 
use Yii;
use yii\data\SqlDataProvider;

class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT me_usuario.nome, COUNT(me_carona.id) AS QUANTIDADE
        FROM me_pedidos JOIN me_usuario.nome ON me_carona.id = 
        WHERE me_carona.id = me_usuario.nome',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }

}
