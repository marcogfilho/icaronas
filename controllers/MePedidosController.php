<?php

namespace app\controllers;

use Yii;
use app\models\MePedidos;
use app\models\MePedidosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MePedidosController implements the CRUD actions for MePedidos model.
 */
class MePedidosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MePedidos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MePedidosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MePedidos model.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($usuario, $carona)
    {
        return $this->render('view', [
            'model' => $this->findModel($usuario, $carona),
        ]);
    }

    /**
     * Creates a new MePedidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MePedidos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario' => $model->usuario, 'carona' => $model->carona]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MePedidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($usuario, $carona)
    {
        $model = $this->findModel($usuario, $carona);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario' => $model->usuario, 'carona' => $model->carona]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MePedidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($usuario, $carona)
    {
        $this->findModel($usuario, $carona)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MePedidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $usuario
     * @param integer $carona
     * @return MePedidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($usuario, $carona)
    {
        if (($model = MePedidos::findOne(['usuario' => $usuario, 'carona' => $carona])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
