<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MeCarona */

$this->title = 'Update Me Carona: ' . $model->idcarona;
$this->params['breadcrumbs'][] = ['label' => 'Adicionar Caronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcarona, 'url' => ['view', 'id' => $model->idcarona]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="me-carona-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
