<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MeCarona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="me-carona-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'veiculo')->dropDownList(['1'=>'Carro','2'=>'Moto'],['prompt' => 'Selecione um veiculo'] );?>

    
    <?= $form->field($model, 'horário')->textInput() ?>

    <?= $form->field($model, 'vagas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model,'oferecedor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
