<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MeCaronaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="me-carona-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idcarona') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'veiculo') ?>

    <?= $form->field($model, 'horário') ?>

    <?= $form->field($model, 'vagas') ?>

    <?php // echo $form->field($model, 'oferecedor') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
