<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MeCarona */

$this->title = $model->idcarona;
$this->params['breadcrumbs'][] = ['label' => 'Me Caronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="me-carona-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->idcarona], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id' => $model->idcarona], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você quer apagar esse item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcarona',
            'data',
            'veiculo',
            'horário',
            'vagas',
            'oferecedor',
        ],
    ]) ?>

</div>
