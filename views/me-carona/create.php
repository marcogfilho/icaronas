<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MeCarona */

$this->title = 'Oferecer Carona';
$this->params['breadcrumbs'][] = ['label' => 'Oferecer Carona', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="me-carona-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
