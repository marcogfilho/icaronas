<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MeUsuario */

$this->title = 'Update Me Usuario: ' . $model->iduser;
$this->params['breadcrumbs'][] = ['label' => 'Me Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iduser, 'url' => ['view', 'id' => $model->iduser]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="me-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
