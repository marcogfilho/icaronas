<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MePontoscarona */

$this->title = 'Update Me Pontoscarona: ' . $model->id_pontos;
$this->params['breadcrumbs'][] = ['label' => 'Me Pontoscaronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pontos, 'url' => ['view', 'id' => $model->id_pontos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="me-pontoscarona-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
