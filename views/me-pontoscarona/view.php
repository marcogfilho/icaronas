<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MePontoscarona */

$this->title = $model->id_pontos;
$this->params['breadcrumbs'][] = ['label' => 'Me Pontoscaronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="me-pontoscarona-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->id_pontos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id' => $model->id_pontos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você quer apagar ese item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pontos',
            'pontos',
        ],
    ]) ?>

</div>
