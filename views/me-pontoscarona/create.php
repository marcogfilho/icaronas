<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MePontoscarona */

$this->title = 'Pontos carona';
$this->params['breadcrumbs'][] = ['label' => 'Pontos caronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="me-pontoscarona-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
