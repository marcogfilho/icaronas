<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MePontoscaronaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pontos caronas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="me-pontoscarona-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar ponto de carona', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pontos',
            'pontos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
