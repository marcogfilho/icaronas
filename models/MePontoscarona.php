<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "me_pontoscarona".
 *
 * @property int $id_pontos
 * @property string $pontos
 */
class MePontoscarona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'me_pontoscarona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pontos'], 'required'],
            [['pontos'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pontos' => 'Id Pontos',
            'pontos' => 'Pontos',
        ];
    }
}
