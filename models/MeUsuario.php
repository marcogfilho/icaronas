<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "me_usuario".
 *
 * @property int $iduser
 * @property string $nome
 * @property string $senha
 *
 * @property MeCarona[] $meCaronas
 * @property MePedidos[] $mePedidos
 * @property MeCarona[] $caronas
 */
class MeUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'me_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
            [['senha'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iduser' => 'ID',
            'nome' => 'Nome',
            'senha' => 'Senha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeCaronas()
    {
        return $this->hasMany(MeCarona::className(), ['oferecedor' => 'iduser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMePedidos()
    {
        return $this->hasMany(MePedidos::className(), ['usuario' => 'iduser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaronas()
    {
        return $this->hasMany(MeCarona::className(), ['idcarona' => 'carona'])->viaTable('me_pedidos', ['usuario' => 'iduser']);
    }
}
