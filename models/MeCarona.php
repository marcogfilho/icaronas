<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "me_carona".
 *
 * @property int $idcarona
 * @property string $data
 * @property string $veiculo
 * @property string $horário
 * @property string $vagas
 * @property int $oferecedor
 *
 * @property MeUsuario $oferecedor0
 * @property MePedidos[] $mePedidos
 * @property MeUsuario[] $usuarios
 */
class MeCarona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'me_carona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'veiculo', 'horário', 'vagas'], 'required'],
            [['data', 'horário'], 'safe'],
            [['oferecedor'], 'integer'],
            [['veiculo'], 'string', 'max' => 15],
            [['vagas'], 'string', 'max' => 4],
            [['oferecedor'], 'exist', 'skipOnError' => true, 'targetClass' => MeUsuario::className(), 'targetAttribute' => ['oferecedor' => 'iduser']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcarona' => 'Idcarona',
            'data' => 'Data',
            'veiculo' => 'Veiculo',
            'horário' => 'Horário',
            'vagas' => 'Vagas',
            'oferecedor' => 'Oferecedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOferecedor0()
    {
        return $this->hasOne(MeUsuario::className(), ['iduser' => 'oferecedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMePedidos()
    {
        return $this->hasMany(MePedidos::className(), ['carona' => 'idcarona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(MeUsuario::className(), ['iduser' => 'usuario'])->viaTable('me_pedidos', ['carona' => 'idcarona']);
    }
}
