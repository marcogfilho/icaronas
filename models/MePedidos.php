<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "me_pedidos".
 *
 * @property int $usuario
 * @property int $carona
 *
 * @property MeUsuario $usuario0
 * @property MeCarona $carona0
 */
class MePedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'me_pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'carona'], 'required'],
            [['usuario', 'carona'], 'integer'],
            [['usuario', 'carona'], 'unique', 'targetAttribute' => ['usuario', 'carona']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => MeUsuario::className(), 'targetAttribute' => ['usuario' => 'iduser']],
            [['carona'], 'exist', 'skipOnError' => true, 'targetClass' => MeCarona::className(), 'targetAttribute' => ['carona' => 'idcarona']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usuario' => 'Usuario',
            'carona' => 'Carona',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(MeUsuario::className(), ['iduser' => 'usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarona0()
    {
        return $this->hasOne(MeCarona::className(), ['idcarona' => 'carona']);
    }
}
